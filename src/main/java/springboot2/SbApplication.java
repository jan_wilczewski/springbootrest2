package springboot2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import springboot2.model.Comment;
import springboot2.model.Post;
import springboot2.repository.CommentRepository;
import springboot2.repository.PostRepository;

@SpringBootApplication
@EnableJpaAuditing
public class SbApplication implements CommandLineRunner{

	@Autowired
	private PostRepository postRepository;
	@Autowired
	private CommentRepository commentRepository;

	public static void main(String[] args) {
		SpringApplication.run(SbApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {

		commentRepository.deleteAllInBatch();
		postRepository.deleteAllInBatch();

		Post post = new Post("fdaf", "dfa", "fda");

		Comment comment1 = new Comment("comment 1");
		comment1.setPost(post);

		Comment comment2 = new Comment("commment 2");
		comment2.setPost(post);

		post.getComments().add(comment1);
		post.getComments().add(comment2);

		postRepository.save(post);
	}
}
