package springboot2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot2.model.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {


}
