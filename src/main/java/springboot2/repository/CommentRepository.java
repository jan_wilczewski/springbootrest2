package springboot2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot2.model.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
}
