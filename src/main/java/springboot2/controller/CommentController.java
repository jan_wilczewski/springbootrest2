package springboot2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springboot2.model.Comment;
import springboot2.repository.CommentRepository;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/ap")
public class CommentController {

    @Autowired
    private CommentRepository commentRepository;

    @GetMapping("/com")
    public List<Comment> getAllComments() {
        return commentRepository.findAll();
    }

    @PostMapping("/com")
    public Comment createComment(@Valid @RequestBody Comment comment) {
        return commentRepository.save(comment);
    }
}
